<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  <meta name="viewport" content="width=1170">
  <meta http-equiv="content-language" content="ru" > <?php // TODO - в будущем генетить автоматом ?>
  <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
  <link rel="icon" type="image/x-icon" href="/favicon.ico">
<?php
  //Регистрируем файлы скриптов в <head>
  if (YII_DEBUG) {
    Yii::app()->assetManager->publish(YII_PATH.'/web/js/source', false, -1, true);
  }

  Yii::app()->clientScript->registerCoreScript('jquery.project');
	Yii::app()->clientScript->registerCoreScript('bootstrap');

 $bootstrapFont = Yii::getPathOfAlias('application.assets.bootstrap.fonts') . DIRECTORY_SEPARATOR;
  Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
    $bootstrapFont.'glyphicons-halflings-regular.eot' =>  '../fonts/',
    $bootstrapFont.'glyphicons-halflings-regular.svg' =>  '../fonts/',
    $bootstrapFont.'glyphicons-halflings-regular.ttf' =>  '../fonts/',
    $bootstrapFont.'glyphicons-halflings-regular.woff' => '../fonts/',
  ));

  Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);

  Yii::app()->clientScript->registerScript('setScroll', "setAnchor();", CClientScript::POS_READY);
  Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);

  Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
  Yii::app()->clientScript->registerCssFile('/themes/business/css/page.css');

?>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<!--[if lt IE 9]>
<link href="/themes/business/css/ie_page.css" media="screen" rel="stylesheet" type="text/css" />
	<![endif]-->
<body>
  <div id="wrap" class="container">
    <div id="head" class="row">
<?php if (Yii::app()->request->url == "/"){ ?>
      <div class="logo col-sm-2"><img border="0" alt="Название компании - На главную" src="/themes/business/gfx/kspp_logo1.png"></div>
<?php } else { ?>
      <a href="/" title="Главная страница" class="logo col-sm-2"><img src="/themes/business/gfx/kspp_logo1.png" alt="Логотип компании"></a>
<?php }?>
      <div class="cname col-sm-7" style="font-family: Georgia;font-weight: bold;line-height: initial;color: darkblue;">
          Региональное объединение работодателей "Коми союз промышленников и предпринимателей"<Br>
      <span style="font-size:19px;">Основан в 1992 году</span>
      </div>
      <div class="tright col-sm-3 pull-right">
        <div class="numbers">
          <p>+7 (8212) <strong>30-10-20</strong></p>
            <br/>
        </div>
      </div>
      <?php $this->widget('ygin.modules.feedback.widgets.FeedbackWidget'); ?>
    </div>
    <div class="b-menu-top navbar navbar-default" role="navigation">
      <div class="navbar-collapse">


<?php

if (Yii::app()->hasModule('search')) {
  $this->widget('SearchWidget');
}
$this->widget('MenuWidget', array(
  'rootItem' => Yii::app()->menu->all,
  'htmlOptions' => array('class' => 'nav navbar-nav'), // корневой ul
  'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
  'activeCssClass' => 'active', // активный li
  'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
  //'labelTemplate' => '{label}', // шаблон для подписи
  'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
  //'linkOptions' => array(), // атрибуты для ссылок
  'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
  'linkDropDownOptionsSecondLevel' => array('data-target' => '#', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
  //'itemOptions' => array(), // атрибуты для li
  'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
  'itemDropDownOptionsSecondLevel' => array('class' => 'dropdown-submenu'),
//  'itemDropDownOptionsThirdLevel' => array('class' => ''),
  'maxChildLevel' => 2,
  'encodeLabel' => false,
));

?>



      </div>
    </div>

<?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>

<?php // + Главный блок ?>
    <div id="main">

      <div id="container" class="row">
<?php

$column1 = 0;
$column2 = 9;
$column3 = 0;

if (Yii::app()->menu->current != null) {
  $column1 = 3;
  $column2 = 6;
  $column3 = 3;

  if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {$column1 = 0; $column3 = 4;}
  if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {$column3 = 0; $column1 = $column1*4/3;}
  $column2 = 12 - $column1 - $column3;
  //if ($column2 == 12) $column2 = 9;
}

?>
        <?php if ($column1 > 0): // левая колонка ?>
        <div id="sidebarLeft" class="col-sm-<?php echo $column1; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
        </div>
        <?php endif ?>

        <div id="content" class="col-sm-<?php echo $column2; ?>">
          <div class="page-header">
            <h1><?php echo $this->caption; ?></h1>
          </div>

          <?php if ($this->useBreadcrumbs && isset($this->breadcrumbs)): // Цепочка навигации ?>
          <?php $this->widget('BreadcrumbsWidget', array(
            'homeLink' => array('Главная' => Yii::app()->homeUrl),
            'links' => $this->breadcrumbs,
          )); ?>
          <?php endif ?>

          <div class="cContent">
            <?php echo $content; ?>
          </div>
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>
        </div>

        <?php if ($column3 > 0): // левая колонка ?>
        <div id="sidebarRight" class="col-sm-<?php echo $column3; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
        </div>
        <?php endif ?>

      </div>
<?php //Тут возможно какие-нить модули снизу ?>
      <div class="clr"></div>
    </div>
<?php // - Главный блок ?>

<div id="back-top"><span>↑</span></div>

  </div>


  <div id="footer" class="container">
    <div class="row footer-inner">
      <div class="col-sm-4 logo">
        <img alt="Логотип компании" src="/themes/business/gfx/kspp_logo1.png">
      </div>
      <div class="col-sm-6">
        <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
      </div>
      <div class="col-sm-2">
        <div id="cvek"><a title="создать сайт в Цифровом веке" href="http://cvek.ru" target="_blank">Создание сайта — веб-студия «Цифровой век»</a></div>
      </div>
    </div>
  </div>


<script type="text/javascript">
if ( $.browser.msie ) {
//fix ie bugs
if ($.browser.version == "8.0" || $.browser.version == "7.0" || $.browser.version == "6.0") {
alert('Для корректной работы сайта обновите, пожалуйста, браузер!');
}
$(".dropdown-toggle").click(function(){
	if ($.browser.version == "8.0"){
	alert('Для корректной работы сайта обновите, пожалуйста, браузер!');
	if ($("#main").attr("style")!=undefined)
	{
		$("#main").removeAttr("style");

	}
	else {$("#main").css("z-index","-1");}
	}
	if ($.browser.version == "7.0")
	{
		alert('Для корректной работы сайта обновите, пожалуйста, браузер!');
		if ($(".dropdown-menu").css("position")=="absolute")
		{
			$(".dropdown-menu").css("position","static");
		}
		else {
			$(".dropdown-menu").css("position","absolute");
		}
	}

	});
}
</script>
</body>
</html>
