<?php
/*
  $currentGallery,
  $childGallery,
*/

$this->caption = $currentGallery->name;
?>
<div class="text">
    <?= $currentGallery->text_in_gallery ?>
</div>
<?php $this->renderPartial('/photogallery_list', array(
  'childGallery' => $childGallery,
));

$this->widget('PhotogalleryWidget', array("model" => $currentGallery));
