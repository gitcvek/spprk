<?php
Yii::import('ygin.modules.banners.widgets.BannerWidget');

class PBannerWidget extends BannerWidget implements IParametersConfig {
  public $bannerPlaceId = Null;
  public $bannerPlaceClass = null;

	public function run() {
    if($this->bannerPlaceId && $this->bannerPlaceClass) {
      $this->widget('ygin.modules.banners.components.BannerJScriptWidget', array(
        'placeId' => $this->bannerPlaceId,
        'placeClass' => $this->bannerPlaceClass,
      ));
    }
	}

  public static function getParametersConfig() {
    return array(
      'bannerPlaceId' => array(
        'type' => DataType::INT,
        'default' => 0,
        'label' => 'Ид баннерного места',
        'required' => true,
      ),
      'bannerPlaceClass' => array(
        'type' => DataType::VARCHAR,
        'default' => 'BannerPlaceWidget',
        'label' => 'Класс баннерного места',
        'required' => false,
      ),
    );
  }

}
