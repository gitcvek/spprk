<?php
Yii::import('ygin.modules.feedback.controllers.FeedbackController');

class MyFeedbackController extends FeedbackController
{
    public function actionIndex() {
        /*$model = BaseActiveRecord::newModel('Feedback');
        $modelClass = get_class($model);

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'feedbackForm') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST[$modelClass])) {
            $model->attributes = $_POST[$modelClass];
            
            if (array_key_exists('g-recaptcha-response', $_POST) && Yii::app()->reCaptcha->isGoodCaptcha($_POST['g-recaptcha-response'])) {
	            $model->onAfterSave = array($this, 'sendMessage'); //Регистрируем обработчик события
	
	            //echo '<pre>'; print_r($_POST[$modelClass]); echo '</pre>';
	
	            $model->id_selo = $_POST[$modelClass]['id_selo'];
	            $model->id_rubric = $_POST[$modelClass]['id_rubric'];
	
	            if ($model->save()) {
	                Yii::app()->user->setFlash('feedback-success', 'Спасибо за Ваше обращение. Ваш вопрос будет рассмотрен в ближайшее время.');
	            } else {
	                // вообще сюда попадать в штатных ситуациях не должны
	                // только если кул хацкер резвится
	                Yii::app()->user->setFlash('feedback-message', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
	            }
            } else {
        		Yii::app()->user->setFlash('feedback-message', 'Подтвердите, пожалуйста, что Вы не робот.');
        	}
        }
        //$this->redirect('/');
        $this->redirect(Yii::app()->user->returnUrl);*/
    	$model = BaseActiveRecord::newModel('Feedback');
    	$modelClass = get_class($model);
    	if (isset($_POST['ajax']) && $_POST['ajax'] === 'feedbackForm') {
      		echo CActiveForm::validate($model);
      		Yii::app()->end();
    	}
    	
    	if (isset($_POST[$modelClass])) {
	      $model->attributes=$_POST[$modelClass];
	      
	      if (array_key_exists('g-recaptcha-response', $_POST) && Yii::app()->reCaptcha->isGoodCaptcha($_POST['g-recaptcha-response'])) {
		      $model->onAfterSave = array($this, 'sendMessage'); //Регистрируем обработчик события
		      if ($model->save()) {
		      	Yii::app()->user->setFlash('feedback-success', 'Спасибо за обращение. Ваше сообщение успешно отправлено.');
		      } else {
		      	// вообще сюда попадать в штатных ситуациях не должны
		      	// только если кул хацкер резвится
		      	Yii::app()->user->setFlash('feedback-message', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
		      }
	      } else {
        	  Yii::app()->user->setFlash('feedback-message', 'Подтвердите, пожалуйста, что Вы не робот.');
          }
    	}
    	$this->redirect(Yii::app()->user->returnUrl);
    }
}
?>