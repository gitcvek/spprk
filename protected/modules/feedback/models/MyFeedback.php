<?php
Yii::import('ygin.modules.feedback.models.Feedback');

class MyFeedback extends Feedback {
	public function rules() {
		return array(
			array('fio, message, mail', 'required'),
			array('mail', 'email', 'message' => 'Введен некорректный e-mail адрес'),
			//array('verifyCode', 'DaCaptchaValidator', 'caseSensitive' => true),
			array('fio, phone, mail, organisation', 'length', 'max'=>255),

            array('message', 'match', 'not' => true, 'pattern' => '/https?:\/\//ui'),
		);
	}
}
?>
